title: "Fanatics"
cover_image: '/images/blogimages/fanatics_case_study_image.png'
cover_title: |
  GitLab offers Fanatics the CI stability they were searching for
cover_description: |
  Fanatics' successful GitLab CI transition empowers innovation cycles and speed
canonical_path: "/customers/fanatics/"
twitter_image: '/images/blogimages/fanatics_case_study_image.png'
customer_logo: '/images/case_study_logos/Fanatics_logo2.svg'
customer_logo_css_class: brand-logo-tall
customer_industry: Retail
customer_location: Jacksonville, FL; San Mateo, CA; Manchester, England; Boulder CO
customer_solution: GitLab CI provided new technology and support that Fanatics was searching for.
customer_employees: 1,800
customer_overview: |
    Fanatics improved CI stability by moving to GitLab.
customer_challenge: |
  The integration team spent too much time working on patches with very little support, creating a need for a stable CI.

key_benefits:
  - |
    CI stability allows the cloud team to focus on innovation instead of simply fighting fires
  - |
    Capability to schedule jobs — which was unavailable in previous tools
  - |
    Increased user happiness ratings to 95%

customer_stats:
  - stat: 800
    label: projects migrated in three months
  - stat: 300
    label: users
  - stat: 0
    label: need for patches

customer_study_content:
  - title: the customer
    subtitle: Bringing the game home to fans
    content:
      - |
        Fanatics is a sports retailer that operates more than 300 online and offline stores. Fanatics offers a tech-infused approach to the world’s largest collection of sports team apparel and jerseys. Their mission is to amplify pride and create connections for all sports fans.

  - title: the challenge
    subtitle: Going around in circles
    content:
      - |
        The Fanatics cloud team has approximately 20 members tasked with all operations pertaining to cloud services and DevOps — including any AWS integrations. The integration team leader is responsible for running the CI/CD pipelines for Fanatics. In late 2018, the team was burdened with working on ongoing issues, such as patches and putting out fires. "It was not a very happy experience,” said Guilherme Goncalves, cloud tech lead. “The support was not very good. We had to solve all the issues ourselves.”
      - |
        Most of his time was spent fixing patches and working to solve issues with their legacy tooling, which included Circle CI. The issues were directly impacting the cloud team and slowed down release times, stopped the deployment of block ends, and caused memory leaks. The whole CI flow was unstable, and especially impacting the cloud team.
      - |
        The POCs were made with several tools like Travis and CodeBuild, but for several reasons like vendor locking, performance, flexibility, and scalability were discarded. It burdened Goncalves’ role to the point that his boss said that if he found a solution, a better tool, then he could make the call to make the switch.

  - title: the solution
    subtitle: Finding stability in a unique tool
    content:
      - |
        Goncalves took his time to find a tool that had the same performance values as their existing tool, but included a level of stability that other tools couldn’t provide. “I researched everything. I looked at the CI tools out there, and I found GitLab and I loved it.”
      - |
        He was the company’s biggest advocate for driving change. He created a GitLab channel, made demos, and asked all the important questions ahead of time. He also implemented multiple POCs in his research for a tool that would be stable and integrate seamlessly into the existing infrastructure. Goncalves encouraged people to experiment for themselves, he said, because GitLab is very powerful.
      - |
        After the push from Goncalves, the decision was made to move over to GitLab at the end of 2018. It took the team about three months to make the entire transition and fully migrate 800 projects. There are now 300 users and about 60 teams using GitLab for CI. “I would say my first three months the cloud team was shifted to entirely GitLab. It was a good investment because GitLab is now running and I don't need to take care of it anymore.”

  - title: the results
    subtitle: Gaining time to focus on business differentiating efforts
    content:
      - |
        With GitLab, the cloud team at Fanatics has gained the ability to focus on innovation instead of worrying about patches and constant issues. “Because GitLab is much more stable, we are able to focus on Fanatics specific challenges, rather than basic infrastructure issues.” GitLab’s support system is responsive and transparent, so if and when issues arise, there is help.
      - |
        The team has started to put more focus on continuous deployments, now that they don’t have to constantly put out fires in the CI world. They can also schedule jobs, which is a feature that Circle CI doesn’t offer. Team members are able to work with group-level environment variables, empowering them to experiment more with workflows and schedule jobs. The benefits to moving to GitLab’s stable CI not only increased the ability to deliver, but allowed the development and engineering teams to collaborate more efficiently.
      - |
        Some teams are experimenting with GitLab’s source code management capabilities and are exploring how GitLab can help with continuous deployment into the future.
      - |
        Overall, Goncalves believes that GitLab would receive over 90% approval rating from fellow users at Fanatics. “Everyone is just happy that their builds are running in a timely fashion and stable enough that they never fail,” Goncalves said.

