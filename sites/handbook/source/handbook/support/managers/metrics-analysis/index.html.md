---
layout: handbook-page-toc
title: Metrics Analysis Workgroup
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# 2020-08
## Attributes
| **Property** | **Value** |
| -- | -- |
| Date Created | 2020-08-13 | 
| Date Ended | |
| Slack | [#spt_metrics-analysis](https://gitlab.slack.com/archives/C018W4ZFGP5) (only accessible from within the company) |
| Google Doc | [Metrics Analysis Workgroup Agenda](https://docs.google.com/document/d/12rzBFFpA5y6xH5PkPcRQF5eSHR03TaAH1JP2jp395VY/edit#heading=h.i3kbcfpbwgta) (only accessible from within the company) | 

## KPIs that triggered this action

## Exit Criteria
- Define conditions under which this workgroup will be reformed.

## Roles and Responsibilities

| **Working Group Role** | **Person** | **Title** | 
| -- | -- | -- |
| Facilitator | Lyle Kozloff | Senior Support Engineering Manager|
| Executive Stakeholder | Tom Cooney | VP of Support |
| Member | Ilia Kosenko | Support Engineering Manager | 
| Member | Wei Meng Lee | Support Engineering Manager | 

## Artifacts


