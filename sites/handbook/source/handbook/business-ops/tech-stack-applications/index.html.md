---
layout: markdown_page
title: "Tech Stack Applications"
extra_js:
  - libs/vue.min.js
  - bundles/tech-stack.js
---

## GitLab Systems Diagram
<img src="https://docs.google.com/drawings/d/e/2PACX-1vSKA7OS_m7lzr1IzZmDcwxACFbz5rifAqMXU0lfJTd4mJhr0t60HgeyfZqEbfeSbwTUEXTbwgCFYJ2t/pub?w=1510&amp;h=766">

#### Tech Stack Support Hierarchy
1. System Owners
1. IT Operations or People Operations

System owners will remain the point of contact for provisioning and issues involving administration. IT Operations engineering empowers system owners to maintain accountability for tech systems through efforts involving automation, monitoring, and visibility. People Operations Specialists supports the tech stack listed in the [People Operations Handbook](/handbook/tools-and-tips/).

#### Tech Stack Lists
* [Tech Stack Google Sheet:](https://docs.google.com/spreadsheets/d/1mTNZHsK3TWzQdeFqkITKA0pHADjuurv37XMuHv12hDU/edit?usp=sharing) contains information about who the business owners, technical owners, provisioners, data gathered, etc
    * Tech Stack definitions: 
        * Business Owner: The Business Owner is responsible for all budget and decision making around the tool. They should define how the tool is used and by whom. This person usually has login access to the tool as `Owner` but login access isn't necessary in all cases.
        * Technical Owner: Are all the `administrators` of a tool. Everyone who has access to provision and deprovision access of a tool as well as the technical expertise needed to manage it. 
        * Provisioner/Deprovisioner: People in charge of granting and removing team member access to a tool. At least two provisioners/deprovisioners should be named for every tool in the handbook. Provisioners can be contacted as a group in Slack using the `@provisioners` handle and in GitLab using the `@tech-stack-provisioner` handle.
* [Tech Stack Overview:](/handbook/business-ops/tech-stack/) provides an overview of how GitLab uses the tool.

#### Tech Stack Issues

- Do you need to update a column in tech stack Google sheet? [Submit an issue.](https://gitlab.com/gitlab-com/business-ops/Business-Operations/issues/new?issuable_template=Update%20Tech%20Stack%20Information)
- Do you need to add a new tool to the tech stack Google sheet? [Submit an issue.](https://gitlab.com/gitlab-com/business-ops/Business-Operations/issues/new?issuable_template=Add%20New%20System%20to%20Tech%20Stack%20Application%20List)
- Do you need to change who is a provisioner to an application? [Submit an issue.](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Update_Tech_Stack_Provisioner)
- Do you need access to an application in the tech stack? Check out [the handbook page](/handbook/business-ops/team-member-enablement/onboarding-access-requests/access-requests/) for a list of the templates you can use to request access.
- Need help with an application? Reach out in the slack channel named in the [Tech Stack google sheet column G](https://docs.google.com/spreadsheets/d/1mTNZHsK3TWzQdeFqkITKA0pHADjuurv37XMuHv12hDU/edit#gid=0) for the application you need help with.
- Do you want to buy a new application? Check out [the handbook page](/handbook/finance/procure-to-pay/#procure-to-pay-process) on the process. Tools may not be integrated into the tech stack without following the process.

Occasionally systems listed in our tech stack will be deprecated. If a system is being offboarded (no longer being used and/or being replaced) an issue will need to be submitted to have the system removed from our tech stack. [Submit and issue.](ttps://gitlab.com/gitlab-com/business-ops/Business-Operations/issues/new?issuable_template=Update%20Tech%20Stack%20Information) Once the issue has been submitted, the Business Owner will need to work with Legal and IT Compliance to ensure that the data deletion process is being completed as outlined in the vendor contract regarding their process and responsibilities. 

#### Additional Information
Also see "Operations License Tracking & Contract Details" which can be found on the Google Drive.

<div id="js-tech-stack-overview"></div>