---
layout: handbook-page-toc
title: "Top misused terms"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Below are terms people frequently use when they should use another term.
The format is: misused term => correct term, reason why.

1. GitLabber => GitLab team member, since the wider community shouldn't be excluded from being a GitLabber, we [use team member instead](/company/team/structure/#team-and-team-members).
1. IPO => becoming a public company, because we might do a direct listing instead of an offering.
1. EE => subscribers or paid users, because [EE is a distribution](/handbook/marketing/product-marketing/tiers/#history-of-ce-and-ee-distributions) and not all people who use EE pay us.
1. CE => Core users, since [CE is a distribution](/handbook/marketing/product-marketing/tiers/#history-of-ce-and-ee-distributions) and many Core users use EE.
1. Hi guys => Hi people, because we want to use [inclusive language](/handbook/values/#inclusive-language--pronouns)
1. Aggressive => ambitious, since we want to [attract a diverse set of people](https://www.huffpost.com/entry/textio-unitive-bias-software_n_7493624)
1. Employees => team members, since we have team members who are [contractors](/handbook/total-rewards/compensation/compensation-calculator/#contract-factor)
1. Resources => people, since we are more than just our output.
1. Community => wider community to refer to people outside of the company, since [people at GitLab are part of the community too](/handbook/communication/#writing-style-guidelines).
1. Radical transparency => Intentional transparency, since radical tends to be absolute and infers a lack of discretion. We are thoughtful and intentional about many things — [informal communication](/company/culture/all-remote/informal-communication/), [handbook-first documentation](/company/culture/all-remote/handbook-first-documentation/), [onboarding](/company/culture/all-remote/building-culture/#intentional-onboarding), etc. — and have [exceptions to transparency](/handbook/communication/#not-public).
1. Sprint => iteration, since we are in it [for the long-term](/handbook/values/#under-construction) and [sprint implies fast, not quick](https://hackernoon.com/iterations-not-sprints-efab8032174c).
1. Grooming => refinement, since the term has [negative connotations](https://pm.stackexchange.com/questions/24133/difference-between-grooming-and-refinement) and the word refinement is used by the [Scrum Guide](https://www.scrumguides.org/revisions.html).
1. Obviously => skip this word, since using things like "obvious/as we all know/clearly" discourages people who don't understand from asking for clarification.
1. They => Do not refer to other teams at GitLab externally as "they" (e.g., "Well, they haven't fixed that yet!").  There is no "they" or "headquarters" - there is only us as team members, and it's one team.  You could instead say something like "The Product team is looking at that issue...". When used in the context of a pronoun, they is as valid as he, she, or any other pronoun someone decides to use.
1. Deprecate => delete or remove, since we usually mean we're removing a document or feature whereas deprecation is actually leaving the thing intact while discouraging its use. Unlike other terms on this list, there *are* times where "deprecate" is the right word, for example in formal [product process](/handbook/product/gitlab-the-product/#deprecating-and-removing-features).
1. Diverse candidate => a single person cannot be "diverse." Using "diverse" as a noun is an “othering” term. Instead, someone may be from an underrepresented group.
1. Non-technical used to describe a person => everyone has a technical knowledge that they used to perform in their role. Usually people mean "non-engineering."
1. Search/Elasticsearch => There are many kinds of search in the GitLab application; use complete feature names to describe areas. Use [Global Search](/direction/enablement/global-search/) with the features Basic Global Search and [Advanced Global Search](https://docs.gitlab.com/ee/user/search/advanced_global_search.html). Use [Log Search](/direction/monitor/apm/logging/) for the monitor stage logging feature. Use Issue Search and Merge Request search for filtering and searching in those content areas. Use [Elastic log Stack](https://docs.gitlab.com/ee/user/clusters/applications.html#elastic-stack) when referring to Elasticsearch and Filebeat.
1. Telemetry => Avoid this word if you can be more specific. You can be more clear by using one of the following terms:
   1. Seat link is number of seats on a license
   1. [Version check](/handbook/sales/process/version-check/) is what version of GitLab a subscription is using
   1. [Usage ping](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html#usage-ping-core-only) is high level data of how GitLab is being used in a license
   1. [Snowplow](/handbook/business-ops/data-team/platform/snowplow/) is a dynamic site for web event analytics
   1. [Google Analytics](/handbook/tools-and-tips/#google-analytics) is a static site for receiving data from `about.gitlab.com` and `docs.gitlab.com`
   1. Database events is using the database records of Gitlab.com to look at how people are using the application.
1. Rule of thumb => Guideline or Heuristic, since rule of thumb is of [dubious historical origins](https://www.phrases.org.uk/meanings/rule-of-thumb.html) and can be considered a violent or non-inclusive term.
1. Deadline => Due date, since deadline [may have violent origins](https://en.wikipedia.org/wiki/Time_limit#Origin_of_the_term) and we use due date in our user interface for issues.
1. Grandfathered => Legacy, since grandfathered has [discriminatory origins](https://en.wikipedia.org/wiki/Grandfather_clause).
1. Whitelist/Blacklist => Allowlist/Denylist, since Whitelist/Blacklist have connotations about value and map to racial terms.
1. Game Day => Simulation Day, since Game Day may seem more militaristic.
1. GitLab monitoring dashboard => GitLab metrics dashboard, monitoring dashboard is too generic and is often confused with the GitLab's self-monitoring features.
1. Second-class citizen => When referencing people, "different class of team member" or "being treated like you don't belong/like an outsider" is preferred when discussing such environments, since citizen has political and socioeconomic connotations. In a [hybrid-remote](/company/culture/all-remote/hybrid-remote/#disadvantages-to-hybrid-remote) environment (e.g. not GitLab, which is [all-remote](/company/culture/all-remote/guide/#why-remote)), those outside of the office may feel inferior or deprioritized compared to coworkers who routinely work onsite.
1. Top/Bottom of the hour => Start of the meeting, while most timezones in the world are offset from UTC by a whole number of hours, a few are offset by [30 or 45 minutes](https://www.timeanddate.com/time/time-zones-interesting.html).
1. Poor man's solution => [Boring solution](/handbook/values/#boring-solutions), since it is gender-specific and implies that someone with less money would come up with a substandard solution compared to someone who has more money.
1. Senior moment => I forgot, we should not associate forgetfulness with senior citizens. 
1. Biweekly/Bimonthly => Twice weekly/monthly or every two weeks/months, since biweekly and bimonthly are ambiguous terms.
1. Functional group leader => [E-group](/handbook/leadership/#e-group) leader, as the term "functional group leader" is a past term that has now been updated. See [Org Structure](/company/team/structure/#organizational-structure).
1. Functional group => Department or team (depending on the context), as "functional group" is a past term that is no longer used. See [Org Structure](/company/team/structure/#organizational-structure).
1. Learning, Playbook, FAQ, Standard Operating Procedure, Training => The entire Handbook is the our standard operating procedure. Using these terms can cause confusion and lead to duplicate content. For example: Call it Contract Negotiation Handbook instead of Contract Negotiation Playbook  
1. White-collar/Blue-collar workers - Use knowledge worker/trade worker instead, the original terms are outdated references and have socioeconomic connotations. There are also other [color classifications](https://en.wikipedia.org/wiki/Designation_of_workers_by_collar_color) that are less common.

