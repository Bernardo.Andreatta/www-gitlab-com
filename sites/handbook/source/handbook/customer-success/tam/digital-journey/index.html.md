### Digital Journey

The digital journey is designed to enable customers to better self-service the educational and “best practices” consulting that Technical Account Managers (TAMs) (link) provide to paying customers. This material is designed to help speed up time-to-value with GitLab, and to help customers adopt workflows and best practices more quickly in order to deepen their return on investment.

Digital journey content is an extension of our existing documentation and community enablement materials ([YouTube](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg), [GitLab blog](https://about.gitlab.com/blog/), [case studies](https://about.gitlab.com/customers/), [forums](https://forum.gitlab.com/), [docs site](https://docs.gitlab.com/) ) designed to help organize the most relevant and most useful resources and help implementation engineers quickly become experts with advice taken from hundreds of existing customers’ experiences and unique environmental or business requirements.

### MVC

Initially the digital journey will take the form of email templates in Gainsight which TAMs can send to customers based on knowing their unique requirements and needs. Each subject will have one or more email templates which can be delivered in a sequence to the individuals responsible for implementing a set of capabilities. These will include a survey of foundational materials, as well as specific advice to make implementations easy and relatively risk free.

**Digital Journey** - [Epic](https://gitlab.com/groups/gitlab-com/account-management/commercial/-/epics/1)  

The email templates will leverage existing materials from YouTube, the GitLab blog, GitLab docs, and other GitLab and 3rd party resources to help customers. However a strong emphasis needs to be placed on making these materials easy for a busy DevSecOps team to consume quickly. As such some longer form materials may need to be adapted to a shorter format for best results.

### Roadmap

WIP - As the catalog of Email Templates grows, and TAMs learn what works, we plan to expand the digital journey to be less reliant on TAMs to send the content, and have more automated content flows based on triggers such as customer stage or use case. The goal is to help all customers equally, regardless of how much they spend on GitLab licenses or services.

### How To Use

TAMs who wish to send Digital Journey emails to their customers have 2 methods:

#### Gainsight Assist gmail plugin  
1.  Install the Chrome plugin [Gainsight Assist](https://chrome.google.com/webstore/detail/gainsight-assist/kbiepllbcbandmpckhoejbgcaddcpbno)  
1.  Configure the plugin domain (just “gitlab” of gitlab.gainsight.com) and login  
1.  Choose “Compose an email” from the Gainsight plugin panel in Gmail  
1.  Search for the template you wish to send  
1.  (optional) Enter Primary Recipient from Gainsight contacts. This will auto-complete any other tokens in the email template based on the Company record for each contact.  
1.  Click “Apply Template” to have a draft email populated in Gmail, ready for you to send to the customer.  

#### CTA with a [Playbook](/handbook/customer-success/tam/gainsight/#ctas)
Some emails intended to be sent as a sequence have been added as Playbooks in Gainsight. These need to be sent manually by the TAM, but are setup to facilitate reminders and make this process easy, while we learn how customers want to engage. To use a playbook to send emails:
1.  In Gainsight, create a new CTA
1.  Type: Lifecycle, choose the Playbook (example:  Commercial Onboarding Email Series). This creates a new CTA with a checklist for each email in the sequence.
1.  Click the whitespace next to the name of the next email to be sent, then on “Validate Email”.
1.  This opens an editor where you can choose contacts and modify the email before sending it.
1.  Click “Preview and Proceed” to see the fully populated and formatted email before sending.
1.  Then send the email (or save as draft).   

To request new materials or capabilities, please use the Commercial-Markets-Initiatives board and CC `@pharlan` for scheduling.
