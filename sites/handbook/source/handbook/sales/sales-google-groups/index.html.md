---
layout: handbook-page-toc
title: "Communicating with the Field"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
This page is meant to serve as a guide to communicating with and within the Field using both Slack and email. Read below to find an outline of all Google Groups and Slack channels related to Sales and Customer Success.

## Google Groups

The parent google group of the sales team is sales-all. Several child groups fall within the parent group. 

| Group Name | Group Level/Access | Description |
| ------ | ------ | ------ |
| sales-all | organization | Includes all members of the sales and CS teams as well as stakeholders across the business |
| sales-commercial | segment | Members of the commercial sales team | 
| sales-ent | segment | Members of the enterprise sales team | 
| sales-emea | team | Members of the ENT EMEA sales team | 
| sales-apac | team | Members of the ENT APAC sales team | 
| sales-east | team | Members of the ENT AMER east sales team | 
| sales-west | team | Members of the ENT AMER west sales team | 
| sales-pubsec | team | Members of the ENT pubsec sales team | 
| sales-cs | segment | Members of the customer success team | 
| sales-channels | segment | Members of the channel partners team | 
| sales-alliances | segment | Members of the alliance partners team | 
| sales-fieldops | segment | Members of the field operations team |
| sales-ops | team | Members of the sales operations team | 
| field-enablement | team | Members of the field enablement team | 
| sales-analytics | team | Members of the sales strategy team | 
| sales-recruiting | segment | People partners and sales recruiters who support the field organization |
| sales-leaders | leadership | Regional directors and above | 
| field-managers | leadership | ASMs and above _or_ equivalents on the CS/Channel/Alliance side who are people managers | 

See the below chart for a visualization of the Google Group hierarchy

![Sales Google Groups](/handbook/sales/images/newsalesgg_v4.jpg)

To confirm if you are in a particular Google Group, see the [G-Suite Group Membership Reports.](/handbook/business-ops/team-member-enablement/#automated-group-membership-reports-for-managers) To gain access to new Google Groups, please [fill out an access request (AR) form.](/handbook/business-ops/team-member-enablement/onboarding-access-requests/access-requests/) 

## Slack Groups

The primary Slack channels for the field are: 

- #field-fyi: Official field announcements channel. Restricted permission levels to help streamline important updates. (Find more information [here](/handbook/sales/sales-google-groups/field-fyi-channel).)
- #sales: The general channel for WW Sales open to posts from all team members.
- #sales-support: The general channel to ask for sales support from Field Operations.
- #customer-success: The general channel to interact with & within Customer Success.

All field team members should star and follow these three channels for important updates about the business. There are multiple team channels in the field organization, outlined below. Please follow the channels relevant to your role. To gain access to private channels, please [fill out an access request (AR) form.](/handbook/business-ops/team-member-enablement/onboarding-access-requests/access-requests/)

### Field Slack Channels
The below channels are specific to the field organization. 

**Organization-Wide/Management**

| Channel Name | Channel Access (Private/Public) | Description |
| ------ | ------ | ------ |
| #sales | public | General channel for WW Sales, open to posts from all team members |
| #field-fyi | public (read-only) | Official field announcements channel |
| #cro | public | CRO (Chief Revenue Officer) announcements and communication |
| #cro-approvals | public | Triaging approvals required by the CRO across GitLab |
| #sales-vp | public | Used for scheduling requests, etc. related to all field VPs |
| #sales-leadership | private | Used for communication with and between field directors and above |
| #sales-managers | public | Used to communicate (non-comp related) topics with field sales managers |
| #sales-ent-managers | private | Used for communication with and between ASM-and-above-level managers in Enterprise |

**Customer Success**

| Channel Name | Channel Access (Private/Public) | Description |
| ------ | ------ | ------ |
| #customer-success | public | Used to interact with & within Customer Success |
| #cs-questions | public | Questions from customers and prospects to the Customer Success team about using GitLab and best practices |
| #demo-systems | public | Open discussion and questions about demo systems that aren’t appropriate for issues |
| #professional-services |  public | Used to communicate with the Professional Services team |

**Field Operations**

| Channel Name | Channel Access (Private/Public) | Description |
| ------ | ------ | ------ |
| #sales-support | public | Support for general sales-related questions, requests, and updates |
| #field-enablement-team | public | Used for communication between GitLab sales and CS team members and the field enablement team |
| #field-cert-program | public | Stay up-to-date on GitLab's [Field Certification Program](/handbook/sales/training/field-certification/) |

**Channels and Alliances**

| Channel Name | Channel Access (Private/Public) | Description |
| ------ | ------ | ------ |
| #alliances | public | Questions regarding alliance partners |
| #channel-sales | public | Topics related to channel sales |
| #channel-programs | public | Topics related to channel programs |
| #channel-marketing | public | Topics related to channel marketing |
| #channel-ops | public | Questions regarding channel operations |
| #channel-services | public | Topics related to channel services |
| #channel-accred-mvp | public | Communication about the channel partner accreditation program |

### Helpful Company Slack Channels
The below channels are owned by teams outside of the field organization but might be helpful for field team members to follow given the nature of their roles. 

| Channel Name | Channel Access (Private/Public) | Description |
| ------ | ------ | ------ |
| #company-fyi | public (read-only) | Official company announcements channel |
| #whats-happening-at-gitlab | public | Team-wide communication and announcements (reminders, events, project updates, etc.) |
| #ceo | public | Questions and requests for Sid |
| #competition | public | Used to share competitive insight/information from the market in real time and to ask the competitive intelligence team questions about how they might handle certain situations |
| #legal | public | General legal questions that are not confidential or seeking legal advice |
| #marketing | public | General marketing team channel |
| #sdr-global | public | Used to ask questions and provide useful information related to XDRs |
| #content-updates | public | The latest GitLab-published content you can share with prospects on social media |
| #newswire | public | News mentions and industry news related to GitLab |
| #diversity_inclusion_and_belonging | public | Get updates and receive feedback on all things related to Diversity, Inclusion & Belonging  |
| #eba-team | public | Communicate with the EA team; schedule time with the CEO and CRO |
