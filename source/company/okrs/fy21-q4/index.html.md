---
layout: markdown_page
title: "FY21-Q4 OKRs"
description: "View GitLabs Objective-Key Results for FY21 Q4. Learn more here!"
canonical_path: "/company/okrs/fy21-q4/"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from November 1, 2020 to January 31, 2021.

## On this page
{:.no_toc}

- TOC
{:toc}

## OKR Schedule
The by-the-book schedule for the OKR timeline would be

| OKR schedule | Week of | Task |
| ------ | ------ | ------ |
| -5 | 2020-09-28 | CEO shares top goals with E-group for feedback |
| -4 | 2020-10-05 | CEO pushes top goals to this page |
| -3 | 2020-10-12 | E-group pushes updates to this page |
| -2 | 2020-10-19 | E-group 50 minute draft review meeting |
| -2 | 2020-10-19 | E-group discusses with teams |
| -1 | 2020-10-26 | CEO reports give a How to Achieve presentation. Pre-Meeting and How to Achieve Presentation recordings are added to this page by EBAs |
| 0  | 2020-11-02 | CoS updates OKR page for current quarter to be active |
| +5 | 2020-12-08 | Review previous and next quarter during the next Board meeting |

## OKRs

### 1. CEO: IACV

### 2. CEO: Popular next generation product

### 3. CEO: Great team


## Pre-Meeting Recordings

* Engineering
* Finance
* Legal
* Marketing
* People
* Product
* Sales

## How to Achieve Presentations

* [Engineering]
* Engineering
* Finance
* Legal
* Marketing
* People
* Product
* Sales
