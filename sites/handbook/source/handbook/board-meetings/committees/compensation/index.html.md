---
layout: handbook-page-toc
title: "Compensation Committee"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## Compensation Committee Composition

* **Chairperson:** Sue Bostrom
* **Members:** Bruce Armstrong, Matthew Jacobson
* **Management DRI:** Chief People Officer

# Compensation Committee Charter
1.	Purpose
The purpose of the Compensation Committee of the Board of Directors (the “Board”) of GitLab Inc. (the “Company”) is to assist the Board in the performance of its responsibilities relating to the Company’s compensation programs in general and specifically, but not limited to, its’ executive officers.
1.	Structure and Membership
    - Number. The Compensation Committee shall consist of at least two members of the Board.
    - Independence. At least two members of the Compensation Committee shall not have management responsibilities.
    - Chair. Unless the Board elects a Chair of the Compensation Committee, the Compensation Committee shall elect a Chair by majority vote.
    - Compensation. The compensation of Compensation Committee members shall be as determined by the Board.
    - Selection and Removal. Members of the Compensation Committee shall be appointed by the Board. The Board may remove members of the Compensation Committee from such committee, with or without cause, at any time that it determines to do so.
1.	Authority and Responsibilities
    - General. The Compensation Committee shall perform its responsibilities, and shall assess the information provided by the Company's management, in accordance with its business judgment.
    - Compensation Matters
    - CEO Compensation and Performance. The Compensation Committee shall annually review and approve corporate goals and objectives relevant to the compensation of the Company’s Chief Executive Officer (the “CEO”), evaluate the CEO’s performance in light of those goals and objectives, and, either as a committee or together with the other independent directors (as directed from time to time by the Board), determine and approve the CEO’s compensation based on this evaluation.
    - Executive Officer Compensation. The Compensation Committee shall review and approve, or recommend for approval by the Board, executive officer (including the CEO) compensation, including salary, bonus and incentive compensation levels; deferred compensation; executive perquisites; equity compensation (including awards to induce employment); severance arrangements;
change-in-control benefits and other forms of executive officer compensation. The Compensation Committee shall meet without the presence of executive officers when approving or deliberating on CEO compensation but may, in its discretion, invite the CEO to be present during approval of, or deliberations with respect to, other executive officer compensation.
1.  Plan Recommendations and Approvals. The Compensation Committee shall periodically review and make recommendations to the Board with respect to incentive-compensation plans and equity-based plans that are subject to approval by the Board.
1.  Director Compensation. The Compensation Committee shall periodically review and make recommendations to the Board of Directors with respect to director compensation.
1.  Additional Powers. The Compensation Committee shall take such other action with respect to compensation matters as may be delegated from time to time by the Board.
1.  Procedures and Administration
    - Meetings. The Compensation Committee shall meet in person or telephonically as often as it deems necessary in order to perform its responsibilities. The Compensation Committee may also act by unanimous written consent in lieu of a meeting. The Compensation Committee shall keep such records of its meetings and furnish the minutes of such meetings to the Board of Directors.
    - Charter. The Compensation Committee shall periodically review and reassess the adequacy of this Charter and recommend any proposed changes to the Board for approval.
    - Compensation Consultants, Legal Counsel and Other Advisors. The Compensation Committee may, in its sole discretion, retain, terminate or obtain the advice of compensation consultants, legal counsel or other advisors. The Compensation Committee shall be directly responsible for the appointment, compensation and oversight of the work of any compensation consultant, legal counsel and other advisor retained by the Compensation Committee. The Compensation Committee is empowered, without further action by the Board, to cause the Company to pay the compensation, as determined by the Compensation Committee, of any
compensation consultant, legal counsel and other advisor retained by the Compensation Committee. The Compensation Committee may select, or receive advice from, a compensation consultant, legal counsel or other advisor, only after taking into consideration, as applicable, all factors relevant to that person’s independence from management.
1.	Investigations. The Compensation Committee shall have the authority to conduct or authorize investigations into any matters within the scope of its responsibilities as it shall deem appropriate, including the authority to request any officer, employee or advisor of the Company to meet with the Compensation Committee or any advisors engaged by the Compensation Committee.
