---
layout: handbook-page-toc
title: Global Benefits Survey
description: Information on GitLab's Global Benefits Survey.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Global Benefits Survey

We've partnered with Culture Amp to run our Global Benefits Survey, which will launch annually in June.  We run this survey to collect feedback from all team members, which will be aggregated and analyzed. Using Culture Amp's reporting tools, we'll analyze the data to find the most common trends for desired benefit iterations. We will use this information to generate a roadmap and share these results with everyone at GitLab.

This is an opportunity to share with the Total Rewards team what benefits you think are great, what benefits are lacking, and what you think is most important to adjust. While we will review and prioritize all feedback, not all input may be implemented in the next few iterations. We will continue to ask the organization what is important to team members, but also take into account our fiduciary responsibility to maintain our operating costs.

**Timeline of the Survey Results:**
* Survey goes out to team members
* Survey closes
* Survey results analyzed
* Survey Results Shared with the GitLab Team
* Survey Results issues opened to begin implementation of any changes
* Q3 OKR to implement changes based on survey results viewed in Q2

Thank you again for playing a part in our efforts to continually improve total rewards at GitLab. If you have any questions, please reach out to the Total Rewards team via email.

### Global Benefits Survey Results 2020

Participation: 62% (0% YOY change)

**Rating Rubric**
5 - strongly agree
4 - agree
3 - neutral
2 - disagree
1 - strongly disagree

#### Benefits Satisfaction Score

The benefits satisfaction score is an average of responses to all ratings questions including general company-wide benefits questions and country-specific questions. Country breakdown only provided for n>4.

By Country:

| Country             | Score |
| --------------------|-------|
| Australia           | 3.68  |
| Austria             | 3.32  |
| Canada              | 3.46  |
| Germany             | 3.52  |
| Hungary             | 3.44  |
| India               | 3.48  |
| Ireland             | 3.06  |
| Mexico              | 3.42  |
| Netherlands         | 3.26  |
| New Zealand         | 3.61  |
| Poland              | 3.59  |
| Romania             | 3.51  |
| Russian Federation  | 3.63  |
| Singapore           | 3.33  |
| South Africa        | 3.79  |
| Turkey              | 3.81  |
| Ukraine             | 3.43  |
| United Kingdom      | 3.48  |
| United States       | 3.69  |

By demographic:

| Demographic            | Score |
|------------------------|-------|
| Male                   | 3.60  |
| Female                 | 3.59  |
| Individual Contributor | 3.64  |
| Manager                | 3.47  |
| Leader                 | 3.49  |
| Company Overall        | 3.60  |

#### General Benefits Survey Responses

1. I understand my benefits package at GitLab.

    | GitLab                 | 2019 Score | 2020 Score | YOY Change |
    |------------------------|------------|------------|------------|
    | Male	                 | 4.16       | 3.89       | -7.63%     |
    | Female                 | 4.21       | 3.87       | -6.86%     |
    | Individual Contributor | 4.13       | 3.88       | -6.15%     |
    | Manager                | 4.35       | 3.88       | -10.90%    |
    | Leader                 | 4.46       | 4.00       | -10.31%    |
    | Company Overall        | 4.18       | 3.88       | -7.07%     |

    By Country:

    | Country             | 2020 Score |
    | --------------------|------------|
    | Australia           | 3.98       |
    | Austria             | 3.40       |
    | Canada              | 3.73       |
    | Germany             | 3.72       |
    | Hungary             | 3.25       |
    | India               | 3.67       |
    | Ireland             | 3.70       |
    | Mexico              | 3.50       |
    | Netherlands         | 3.56       |
    | New Zealand         | 4.18       |
    | Poland              | 4.00       |
    | Romania             | 3.86       |
    | Russian Federation  | 4.00       |
    | Singapore           | 3.67       |
    | South Africa        | 3.40       |
    | Turkey              | 3.60       |
    | Ukraine             | 3.40       |
    | United Kingdom      | 3.86       |
    | United States       | 3.99       |

1. The general benefits at GitLab are equal to or better than what is offered by similar employers.

    | GitLab                 | 2019 Score | 2020 Score | YOY Change |
    |------------------------|------------|------------|------------|
    | Male	                 | 3.67       | 3.42       | -6.90%     |
    | Female                 | 3.66       | 3.46       | -5.58%     | 
    | Individual Contributor | 3.72       | 3.50       | -5.80%     |
    | Manager                | 3.33       | 3.23       | -2.98%     | 
    | Leader                 | 3.46       | 3.12       | -9.96%     |
    | Company Overall        | 3.66       | 3.43       | -6.31%     | 

    By Country:

    | Country             | 2020 Score |
    | --------------------|------------|
    | Australia           | 3.51       |
    | Austria             | 2.20       |
    | Canada              | 3.17       |
    | Germany             | 3.24       |
    | Hungary             | 3.13       |
    | India               | 3.00       |
    | Ireland             | 2.57       |
    | Mexico              | 2.67       |
    | Netherlands         | 2.75       |
    | New Zealand         | 3.55       |
    | Poland              | 3.58       |
    | Romania             | 3.29       |
    | Russian Federation  | 3.00       |
    | Singapore           | 2.67       |
    | South Africa        | 3.67       |
    | Turkey              | 3.40       |
    | Ukraine             | 3.60       |
    | United Kingdom      | 3.18       |
    | United States       | 3.64       |

1. The general benefits at GitLab save me a great deal of time and/or money, and add significant value to my employee experience.

    | GitLab                 | 2019 Score | 2020 Score | YOY Change |
    |------------------------|------------|------------|------------|
    | Male	                 | 3.75       | 3.65       | -2.66%     |
    | Female                 | 3.79       | 3.59       | -5.40%     |
    | Individual Contributor | 3.83       | 3.70       | -3.39%     |
    | Manager                | 3.38       | 3.41       |  0.84%     |
    | Leader                 | 3.54       | 3.43       | -3.00%     |
    | Company Overall        | 3.77       | 3.63       | -3.71%     |

    By Country:

    | Country             | 2020 Score |
    | --------------------|------------|
    | Australia           | 3.67       |
    | Austria             | 3.20       |
    | Canada              | 3.40       |
    | Germany             | 3.86       |
    | Hungary             | 3.75       |
    | India               | 3.73       |
    | Ireland             | 3.00       |
    | Mexico              | 3.50       |
    | Netherlands         | 2.96       |
    | New Zealand         | 3.36       |
    | Poland              | 3.42       |
    | Romania             | 3.71       |
    | Russian Federation  | 3.33       |
    | Singapore           | 2.83       |
    | South Africa        | 3.50       |
    | Turkey              | 3.60       |
    | Ukraine             | 3.60       |
    | United Kingdom      | 3.21       |
    | United States       | 3.79       |

1. My benefits package provides quality coverage for myself and, if applicable, my dependents.

    | GitLab                 | 2019 Score | 2020 Score | YOY Change |
    |------------------------|------------|------------|------------|
    | Male	                 | 3.59       | 3.82       |   6.45%    |
    | Female                 | 3.63       | 3.72       |   2.39%    |
    | Individual Contributor | 3.63       | 3.77       |   3.86%    |
    | Manager                | 3.38       | 3.83       |  13.36%    |
    | Leader                 | 3.58       | 3.81       |   6.41%    |
    | Company Overall        | 3.60       | 3.79       |   5.14%    | 

    By Country:

    | Country             | 2020 Score |
    | --------------------|------------|
    | Canada              | 3.68       |
    | India               | 2.07       |
    | United Kingdom      | 3.06       |
    | United States       | 3.94       |

1. The wellness offerings at GitLab help me lead a happier, healthier life.

    | GitLab                 | 2019 Score | 2020 Score | YOY Change |
    |------------------------|------------|------------|------------|
    | Male	                 | 3.39       | 3.23       | -4.86%     |
    | Female                 | 3.28       | 3.28       | -0.01%     |
    | Individual Contributor | 3.41       | 3.31       | -3.07%     |
    | Manager                | 3.26       | 3.01       | -7.76%     |
    | Leader                 | 2.92       | 3.18       |  8.78%     |
    | Company Overall        | 3.36       | 3.24       | -3.50%     |

    By Country:

    | Country             | 2020 Score |
    | --------------------|------------|
    | Australia           | 3.07       |
    | Canada              | 3.36       |
    | Germany             | 2.74       |
    | Hungary             | 2.88       |
    | India               | 3.27       |
    | Ireland             | 2.57       |
    | Mexico              | 3.33       |
    | Netherlands         | 2.74       |
    | New Zealand         | 3.20       |
    | Poland              | 2.83       |
    | Romania             | 3.14       |
    | Russian Federation  | 2.83       |
    | Singapore           | 3.50       |
    | South Africa        | 4.17       |
    | Turkey              | 3.80       |
    | Ukraine             | 3.20       |
    | United Kingdom      | 3.04       |
    | United States       | 3.41       |

1. I believe our benefits package is one of the top reasons people apply to work at GitLab.

    | GitLab                 | 2019 Score | 2020 Score | YOY Change |
    |------------------------|------------|------------|------------|        
    | Male	                 | 2.87       | 3.04       |  5.87%     |
    | Female                 | 2.81       | 3.16       | 12.40%     |
    | Individual Contributor | 2.96       | 3.18       |  7.38%     |
    | Manager                | 2.40       | 2.76       | 14.87%     |
    | Leader                 | 2.35       | 2.79       | 18.83%     |
    | Company Overall        | 2.86       | 3.08       |  7.55%     |

    By Country:

    | Country             | 2020 Score |
    | --------------------|------------|
    | Australia           | 3.19       |
    | Canada              | 2.57       |
    | Germany             | 3.24       |
    | Hungary             | 3.63       |
    | India               | 3.07       |
    | Ireland             | 2.48       |
    | Mexico              | 3.00       |
    | Netherlands         | 2.68       |
    | New Zealand         | 3.09       |
    | Poland              | 3.33       |
    | Romania             | 2.71       |
    | Russian Federation  | 2.67       |
    | Singapore           | 2.17       |
    | South Africa        | 3.40       |
    | Turkey              | 3.20       |
    | Ukraine             | 2.80       |
    | United Kingdom      | 2.73       |
    | United States       | 3.19       |

1. Should I have or care for a(nother) child, the parental leave policy is sufficient.

    | GitLab                 | 2019 Score | 2020 Score | YOY Change |
    |------------------------|------------|------------|------------|
    | Male	                 | 3.95       | 3.95       |  0.09%     |
    | Female                 | 3.44       | 3.60       |  4.60%     |
    | Individual Contributor | 3.82       | 3.84       |  0.49%     |
    | Manager                | 3.61       | 3.89       |  7.68%     |
    | Leader                 | 4.04       | 3.80       | -6.04%     |
    | Company Overall        | 3.81       | 3.84       |  0.91%     |

    By Country:

    | Country             | 2020 Score |
    | --------------------|------------|
    | Australia           | 3.85       |
    | Austria             | 3.80       |
    | Canada              | 3.50       |
    | Germany             | 3.62       |
    | Hungary             | 3.88       |
    | India               | 3.79       |
    | Ireland             | 3.39       |
    | Mexico              | 4.00       |
    | Netherlands         | 3.57       |
    | New Zealand         | 3.44       |
    | Poland              | 3.64       |
    | Romania             | 3.00       |
    | Russian Federation  | 3.50       |
    | Singapore           | 3.33       |
    | South Africa        | 4.33       |
    | Turkey              | 4.60       |
    | Ukraine             | 3.20       |
    | United Kingdom      | 3.80       |
    | United States       | 3.97       |

1. The vacation policy allows me sufficient time to recharge.

    | GitLab                 | 2019 Score | 2020 Score | YOY Change |
    |------------------------|------------|------------|------------|
    | Male	                 | 4.48       | 4.39       | -2.09%     | 
    | Female                 | 4.40       | 4.37       | -0.68%     |
    | Individual Contributor | 4.49       | 4.43       | -1.29%     |
    | Manager                | 4.27       | 4.24       | -0.79%     |
    | Leader                 | 4.38       | 4.21       | -3.94%     |
    | Company Overall        | 4.46       | 4.38       | -1.76%     |

    By Country:

    | Country             | 2020 Score |
    | --------------------|------------|
    | Australia           | 4.49       |
    | Austria             | 4.80       |
    | Canada              | 4.30       |
    | Germany             | 4.21       |
    | Hungary             | 4.50       |
    | India               | 4.33       |
    | Ireland             | 3.83       |
    | Mexico              | 4.00       |
    | Netherlands         | 4.40       |
    | New Zealand         | 4.36       |
    | Poland              | 4.42       |
    | Romania             | 4.43       |
    | Russian Federation  | 4.50       |
    | Singapore           | 4.50       |
    | South Africa        | 4.67       |
    | Turkey              | 4.60       |
    | Ukraine             | 4.80       |
    | United Kingdom      | 4.47       |
    | United States       | 4.40       |

1. I believe our benefits package is one of the top reasons why people stay at GitLab.

    | GitLab                 | 2019 Score | 2020 Score | YOY Change |
    |------------------------|------------|------------|------------|
    | Male	                 | 3.08       | 3.24       |  5.21%     |
    | Female                 | 3.13       | 3.31       |  5.77%     |
    | Individual Contributor | 3.21       | 3.35       |  4.36%     |
    | Manager                | 2.58       | 3.00       | 16.28%     |
    | Leader                 | 2.54       | 2.98       | 17.37%     |
    | Company Overall        | 3.09       | 3.26       |  5.58%     |

    By Country:

    | Country             | 2020 Score |
    | --------------------|------------|
    | Australia           | 3.30       |
    | Austria             | 2.80       |
    | Canada              | 2.80       |
    | Germany             | 3.41       |
    | Hungary             | 3.25       |
    | India               | 3.07       |
    | Ireland             | 2.65       |
    | Mexico              | 3.17       |
    | Netherlands         | 2.79       |
    | New Zealand         | 3.27       |
    | Poland              | 3.50       |
    | Romania             | 3.14       |
    | Russian Federation  | 3.50       |
    | Singapore           | 2.33       |
    | South Africa        | 3.50       |
    | Turkey              | 3.20       |
    | Ukraine             | 2.60       |
    | United Kingdom      | 3.04       |
    | United States       | 3.38       |

1. I believe investing more of the company’s money into improving benefits at GitLab will help to attract and retain talent.

    | GitLab                 | 2019 Score | 2020 Score | YOY Change |
    |------------------------|------------|------------|------------|
    | Male	                 | 4.16       | 4.13       | -0.68%     |
    | Female                 | 4.25       | 4.16       | -2.07%     |
    | Individual Contributor | 4.20       | 4.16       | -0.97%     |
    | Manager                | 4.25       | 4.11       | -3.27%     |
    | Leader                 | 3.85       | 4.02       |  4.39%     |
    | Company Overall        | 4.18       | 4.14       | -0.93%     | 

    By Country:

    | Country             | 2020 Score |
    | --------------------|------------|
    | Australia           | 3.98       |
    | Austria             | 4.40       |
    | Canada              | 3.97       |
    | Germany             | 4.29       |
    | Hungary             | 4.13       |
    | India               | 4.33       |
    | Ireland             | 4.57       |
    | Mexico              | 4.00       |
    | Netherlands         | 3.92       |
    | New Zealand         | 3.82       |
    | Poland              | 3.83       |
    | Romania             | 3.43       |
    | Russian Federation  | 4.50       |
    | Singapore           | 4.33       |
    | South Africa        | 4.33       |
    | Turkey              | 4.60       |
    | Ukraine             | 3.40       |
    | United Kingdom      | 4.33       |
    | United States       | 4.12       |

1. When I take time off, I don't feel it is necessary to check in with work periodically.

    | GitLab                 | 2020 Score |
    |------------------------|------------|
    | Male	                 | 3.67       |
    | Female                 | 3.40       |
    | Individual Contributor | 3.68       |
    | Manager                | 3.33       |
    | Leader                 | 3.23       |
    | Company Overall        | 3.59       |

    By Country:

    | Country             | 2020 Score |
    | --------------------|------------|
    | Australia           | 4.05       |
    | Austria             | 3.40       |
    | Canada              | 3.40       |
    | Germany             | 3.79       |
    | Hungary             | 3.25       |
    | India               | 4.20       |
    | Ireland             | 2.96       |
    | Mexico              | 2.83       |
    | Netherlands         | 3.52       |
    | New Zealand         | 3.55       |
    | Poland              | 3.83       |
    | Romania             | 3.71       |
    | Russian Federation  | 4.00       |
    | Singapore           | 4.00       |
    | South Africa        | 4.00       |
    | Turkey              | 4.00       |
    | Ukraine             | 3.60       |
    | United Kingdom      | 3.53       |
    | United States       | 3.50       |

1. I understand how our PTO works in sync with my local statutory PTO entitlement.

    | GitLab                 | 2020 Score |
    |------------------------|------------|
    | Male	                 | 3.98       |
    | Female                 | 4.02       |
    | Individual Contributor | 4.02       |
    | Manager                | 3.85       |
    | Leader                 | 4.12       |
    | Company Overall        | 3.99       |

    By Country:

    | Country             | 2020 Score |
    | --------------------|------------|
    | Australia           | 3.79       |
    | Austria             | 3.60       |
    | Canada              | 4.00       |
    | Germany             | 4.00       |
    | Hungary             | 4.00       |
    | India               | 4.47       |
    | Ireland             | 3.78       |
    | Mexico              | 3.83       |
    | Netherlands         | 4.08       |
    | New Zealand         | 4.18       |
    | Poland              | 4.00       |
    | Romania             | 4.43       |
    | Russian Federation  | 4.67       |
    | Singapore           | 3.50       |
    | South Africa        | 3.33       |
    | Turkey              | 4.20       |
    | Ukraine             | 4.20       |
    | United Kingdom      | 3.88       |
    | United States       | 4.04       |

1. I believe our Employee Assistance Plan offered by Modern Health is a valuable benefit worth retaining.

    | GitLab                 | 2020 Score |
    |------------------------|------------|
    | Male	                 | 3.10       |
    | Female                 | 3.40       |
    | Individual Contributor | 3.20       |
    | Manager                | 3.14       |
    | Leader                 | 3.22       |
    | Company Overall        | 3.19       |

    By Country:

    | Country             | 2020 Score |
    | --------------------|------------|
    | Australia           | 3.21       |
    | Austria             | 3.00       |
    | Canada              | 2.93       |
    | Germany             | 2.54       |
    | Hungary             | 2.86       |
    | India               | 3.14       |
    | Ireland             | 2.91       |
    | Mexico              | 3.80       |
    | Netherlands         | 2.70       |
    | New Zealand         | 3.00       |
    | Poland              | 2.67       |
    | Romania             | 3.14       |
    | Russian Federation  | 2.40       |
    | Singapore           | 3.33       |
    | South Africa        | 4.00       |
    | Ukraine             | 2.40       |
    | United Kingdom      | 3.20       |
    | United States       | 3.37       |

1. If I have questions or concerns regarding benefits, I know whom to reach out to.

    | GitLab                 | 2020 Score |
    |------------------------|------------|
    | Male	                 | 3.85       |
    | Female                 | 3.78       |
    | Individual Contributor | 3.77       |
    | Manager                | 3.97       |
    | Leader                 | 4.08       |
    | Company Overall        | 3.82       |

    By Country:

    | Country             | 2020 Score |
    | --------------------|------------|
    | Australia           | 4.02       |
    | Austria             | 3.60       |
    | Canada              | 3.90       |
    | Germany             | 3.61       |
    | Hungary             | 3.25       |
    | India               | 3.73       |
    | Ireland             | 3.65       |
    | Mexico              | 3.50       |
    | Netherlands         | 3.52       |
    | New Zealand         | 3.91       |
    | Poland              | 3.75       |
    | Romania             | 4.14       |
    | Russian Federation  | 4.17       |
    | Singapore           | 3.50       |
    | South Africa        | 4.50       |
    | Turkey              | 3.60       |
    | Ukraine             | 3.40       |
    | United Kingdom      | 3.77       |
    | United States       | 3.87       |

1. What has worked best in convincing me to take PTO?

    | Response                                                                       | Count | % of Respondents |
    |--------------------------------------------------------------------------------|-------|------------------|
    | Stress                                                                         | 333   | 42%              |
    | Example from my manager.                                                       | 391   | 50%              |
    | Encouragement from my manager/peers to support my wellbeing through time off.  | 475   | 61%              |
    | Example from leaders, for example Sid.                                         | 456   | 58%              |
    | Reduced workload.                                                              | 101   | 13%              |
    | Other (Expanded on below.)                                                     | 118   | 15%              |

    * Summary of Other Responses:
      * When I feel it's needed. 
      * Family first/family time.
      * Addressing burn out/taking time to rest.
      * Example from peers.
      * PTO Ninja reminders.
      * Friends and Family Day specifically.

1. I participated in the Simple Habit free trial and would like to see this benefit offered permanently (survey respondents were instructed to select one).

    | Response                                                                                                                               | Count | % of Respondents |
    |----------------------------------------------------------------------------------------------------------------------------------------|-------|------------------|
    | Yes, I participated and agree with the statement above.                                                                                | 15    | 2%               | 
    | No, I did not participate, but am interested in GitLab offering a meditation and mental-wellness benefit in addition to Modern Health. | 500   | 64%              | 
    | Yes, I participated and do not agree with the statement above.                                                                         | 6     | 1%               | 
    | No, I did not participate and am not interested in GitLab offering a mediation and mental-wellness benefit.                            | 258   | 33%              | 

1. What corporate social responsibility activities would you like to see GitLab enter? The purpose of corporate social responsibility is to give back to the community, take part in philanthropic causes, and provide positive social value. 
   * Summary of Responses:
     * Giving team members a set amount of money to donate to causes they support each year.
     * Donation matching for donations made by team members or hours volunteered.
     * Community/Volunteer Day(s) similar to Friends & Family Day where everyone has the day off.
     * Youth initiatives including supporting underrepresented, underprivileged, and/or minority youth in their education, coding, mentorship, etc.
     * Donating to and supporting causes that support ending inequality such as causes that support LGBTQ+ people, Black Lives Matter, organizations that support minorities and minorities in tech, etc.
     * Coding initiatives especially for women and minorities. 
     * Donating to and supporting environmental causes, reducing our carbon footprint, creating an incentive program for team members to be more green. 
     * Supporting open source projects. 
     * Donating free licenses and GitLab support/training to charitable organizations.
     * Providing mentorships and internships in support of D&I initiatives. 

#### Entity Specific Questions


1. The country-specific benefits provided by GitLab are comparable or better than what is offered by similar employers in my country.

    | GitLab                 | Australia Score |
    |------------------------|-----------------|
    | Male	                 | 3.21            |
    | Female                 | 4.67            |
    | Individual Contributor | 3.50            |
    | Manager                | 2.89            |
    | Leader                 | 2.50            |
    | Overall                | 3.32            |

1. The benefits provided by Safeguard are comparable or better than what is offered by similar employers.

    | GitLab                 | Ireland Score   |
    |------------------------|-----------------|
    | Male	                 | 1.54            |
    | Female                 | 1.50            |
    | Individual Contributor | 1.59            |
    | Manager                | 1.25            |
    | Leader                 | 1.50            |
    | Overall                | 1.52            |

1. I am satisfied with my employment through Safeguard and know who to contact if any problems were to arise.

    | GitLab                 | Ireland Score   |
    |------------------------|-----------------|
    | Male	                 | 2.38            |
    | Female                 | 2.20            |
    | Individual Contributor | 2.35            |
    | Manager                | 2.25            |
    | Leader                 | 2.00            |
    | Overall                | 2.30            |

1. Out of the following options, which benefit would you most like to see GitLab offer in the future? (select up to 4)


    | Response                                                          | Australia (Count) | Australia (% of Respondents) | Ireland (Count) | Ireland (% of Respondents) |
    |-------------------------------------------------------------------|-------------------|------------------------------|-----------------|----------------------------|
    | Paid parental leave above 16 weeks.                               | 10                | 23%                          | 6               | 26%                        | 
    | 100% paid medical leave for 16 weeks.                             | 6                 | 14%                          | 6               | 26%                        |
    | Expanded mental health offerings (ie. Therapy, meditation, etc.)  | 7                 | 16%                          | 8               | 35%                        |
    | Wellness stipend.                                                 | 18                | 42%                          | 9               | 39%                        |
    | Childcare stipend.                                                | 4                 | 9%                           | 5               | 22%                        |
    | Student loan assistance program.                                  | 2                 | 5%                           | 2               | 9%                         |
    | Health insurance.                                                 | 26                | 60%                          | n/a             | n/a                        |
    | Parental leave transition coaching.                               | 1                 | 2%                           | 0               | 0%                         |
    | Ergonomics consultations.                                         | 2                 | 5%                           | 1               | 4%                         |
    | Global Pension with employer match.                               | 9                 | 21%                          | 18              | 78%                        | 
    | Annual company-wide bonus.                                        | 23                | 53%                          | 11              | 48%                        |
    | Charitable giving.                                                | 12                | 28%                          | 3               | 13%                        |

1. Which benefit would you be most  willing to sacrifice to allow for new benefits?

    | Response                                                          | Australia (Count) | Australia (% of Respondents) | Ireland (Count) | Ireland (% of Respondents) |
    |-------------------------------------------------------------------|-------------------|------------------------------|-----------------|----------------------------|
    | Paid parental leave.                                              | 4                 | 9%                           | 0               | 0%                         |
    | Employee Assistance Program (Modern Health).                      | 11                | 26%                          | 4               | 17%                        |
    | Office supplies/furniture reimbursement.                          | 2                 | 5%                           | 0               | 0%                         |
    | Tuition Reimbursement.                                            | 4                 | 9%                           | 5               | 22%                        |
    | Business Travel Accident Policy.                                  | 2                 | 5%                           | 1               | 4%                         |
    | Discretionary Bonuses.                                            | 6                 | 14%                          | 2               | 9%                         |
    | Referral Bonuses.                                                 | 1                 | 2%                           | 3               | 13%                        |
    | Stock options                                                     | 2                 | 5%                           | 0               | 0%                         |
    | Visiting grant                                                    | 2                 | 5%                           | 4               | 17%                        |
    | $20,000 Life Insurance (Expression of Wishes)                     | 8                 | 19%                          | 3               | 13%                        | 

1. Which benefits do you most want GitLab to retain? (select up to 4)

    | Response                                                          | Australia (Count) | Australia (% of Respondents) | Ireland (Count) | Ireland (% of Respondents) |
    |-------------------------------------------------------------------|-------------------|------------------------------|-----------------|----------------------------|
    | Paid parental leave.                                              | 17                | 40%                          | 13              | 57%                        |
    | Employee Assistance Program (Modern Health).                      | 7                 | 16%                          | 1               | 4%                         | 
    | Office supplies/furniture reimbursement.                          | 29                | 67%                          | 15              | 65%                        |
    | Tuition Reimbursement.                                            | 10                | 23%                          | 8               | 35%                        |   
    | Business Travel Accident Policy.                                  | 8                 | 19%                          | 1               | 4%                         |
    | Discretionary Bonuses.                                            | 21                | 49%                          | 9               | 39%                        |
    | Referral Bonuses.                                                 | 1                 | 2%                           | 5               | 22%                        |
    | Stock options                                                     | 33                | 77%                          | 20              | 87%                        |
    | Visiting grant                                                    | 21                | 49%                          | 0               | 0%                         |
    | $20,000 Life Insurance (Expression of Wishes)                     | 2                 | 5%                           | 3               | 13%                        |

**Action Items/Issues to Open:**

TODO

## Benefits Not Currently Being Implemented

This section serves to highlight benefits that we have previously researched, but have decided not to move forward with implementing at this time. As the company grows, circumstances change, and/or we receive new information, we may decide to revisit any benefits added to this list.

### Telehealth

We researched and selected four vendors to receive more information. Demo calls were conducted with three of these vendors where we learned more about the solutions and pricing. After reviewing the [results of the benefits survey](/handbook/total-rewards/benefits/benefits-survey/#global-benefits-survey-results), there wasn’t enough interest in a telehealth solution to justify the price so we decided to not move forward with any of the vendors at this time.

While we aren't offering a global telehealth solution at this time, team members based in the US who are enrolled in our [UHC](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#uhc-telehealth) or [Kaiser](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#kaiser-telehealth) medical plans can access telehealth services through these plans. Other team members may have options for telehealth available to them through their provider.

Further information and corresponding discussion are available under [Compensation Issue #15](https://gitlab.com/gitlab-com/people-ops/Compensation/issues/15) (internal).

### Coworking Space Agreement

We researched whether there was an account that could be set up with a coworking space chain that would make it easier for team members to utilize these spaces. Three major chains with global coverage were contacted with one responding. After conducting a survey, the type of account this chain offers doesn't align with the way the majority of team members utilize coworking spaces. Team members are still welcome to follow the existing process of [expensing a coworking space](/handbook/finance/expenses/#coworking-or-external-office--space).

Further information, survey results, and corresponding discussion are available under [Compensation Issue #30](https://gitlab.com/gitlab-com/people-ops/Compensation/issues/30) (internal).

### Wellness Program

We researched and selected three vendors and conducted a cost analysis with Finance. At this time, it does not fit into the budget for benefits to be offered for FY21, but will be revisited when we evaluate benefits and budget for the next fiscal year. Further information and corresponding discussion are available under [Compensation Issue #37](https://gitlab.com/gitlab-com/people-group/Compensation/-/issues/37) (internal).
