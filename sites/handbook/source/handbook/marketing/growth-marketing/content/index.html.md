---
layout: handbook-page-toc
title: "Global Content Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What does the Global Content Team do?

The Global Content team is a [sub-department of the Growth Marketing within the Marketing divison](/company/team/structure/#organizational-structure) and includes three (3) teams: content marketing, editorial,  digital production. Our team is responsible for content strategy, content development, content operations, and localization. 

## Slack Channels

- `#content` for general inquiries
- `#content-updates` for updates and log of all new, published content
- `#content-hack-day` for updates and information on Content Hack Day
- `#blog` RSS feed of published posts

## Teams

- [Content Marketing](/handbook/marketing/growth-marketing/content/content-marketing/)
- [Digital Production](/handbook/marketing/growth-marketing/content/digital-production/)
- [Editorial](/handbook/marketing/growth-marketing/content/editorial-team)

## Quick links

- [GitLab blog handbook](/handbook/marketing/blog)
- [Gated content process](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/)
- [Newsletter process](/handbook/marketing/marketing-sales-development/marketing-programs/#newsletter)

## Editorial calendar

<iframe src="https://calendar.google.com/calendar/embed?src=gitlab.com_ame9843c6094ffc475vea9ftn4%40group.calendar.google.com&ctz=America%2FDenver" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>

### Adding an event to the editorial calendar

1. Make sure there is an associated issue for the event. Link the issue in the event description.
1. Check "all day event" unless there is a specific time the piece should publish. This is rare and usually only if a blog post is tied to a time-sensitive PR announcement.
1. Homepage hero content is a multi-day/week long event. Make sure it is listed on the calendar for the entirity of it's promotional period.
1. Name your event using the following nomenclature: <emoji, title of content, campaign>. Example: `📝 5 Reasons to Use GitLab, Just Commit`

**Legend**

- 📣 Announcement
- 📝 Blog post
- 🔍 Case Study 
- 📚 eBook/Whitepaper
- 📍 Homepage Hero Content 
- ℹ️ Infographic
- 🎙️ Podcast
- 📈 Report
- 📹 Video
- 🌐 Webpage

## Goals & Key Deliverables 

As a sub-department of Growth Marketing, all of our activites are aligned to the Growth Team's goals. Our team is primarily focused on content output and optimizing the performance of our content. Here's how the Global Content Team's activities support our goals.

### Increasing GitLab.com website traffic 20% YoY 

1. Publishing search optimized content to the GitLab blog. 
1. Publishing search optimized topic pages and web articles to the GitLab marketing website. 
1. Publishing video content to attract visitors. 
1. Publishing links to our marketing site from YouTube videos. 

### Delivering inbound MQLs 

1. Publishing gated content like eBooks, whitepapers, and research reports.
1. Supporting integrated campaigns with content journey maps, nurture emails, and supplementary content. 
1. Copyediting and optimizing website content for conversions. 

### Increasing conversions of free GitLab.com users to paying customers

1. Publishing content that helps users understand how to get the most out of GitLab.
1. Supporting the Growth Product team with content for product trials and in-app messaging. 

## Content Strategy

Embrace a media company mindset to develop a content strategy that organically attracts and retains our target audience over the use of traditional disruption tactics with the goal of increasing website traffic and preference for our content resources. Our global content strategy prioritizes the needs and preferences of our audiences over selling GitLab in order to build trust and increase audience retention. We aim to create enjoyable content experiences that people seek out. 

We accomplish this through: 

1. Rigorous research and understanding of our audience needs, behaviors, and lived experiences. 
1. A single content hub that is easy to find, search, and interact with. 
1. Consistency in quality and format. Our audience knows what to expect. 
1. A mix of journalistic and entertainment style content. 
1. Exceptional communication standards. We prioritize a clear and direct tone of voice over cutsy, convoluted language. 

### Content Strategy Principles 

1. **Audience-first mindset.**  All content is produced with the audience in mind and have a clearly definited "who, what, why, how". 
1. **Entertain and connect.** Our content is entertaining, enjoyable to consume, and expresses our acute understanding our audience. We want our content to compell our audience to binge and share versus bookmark for later. 
1. **Make it easy.** Relevant content is easy to find regardless of format. Our content finds our audiences where they are. 
1. **Media company mindset.** Our global content team operates with the mindset that our audience is our customers, content is our product, and subscribers are our currency. 

## FY21-22 Direction

### Blog

The Editorial team's [goals are to grow our blog audience, engage readers, and covert readers into subscribers](/handbook/marketing/growth-marketing/content/editorial-team/#goal-grow-our-audience-engage-readers-and-convert-readers-into-subscribers). To achieve this our strategy is guided by our existing audience's needs and wants; prioritizing useful, educational, informative, and entertaining content that has value beyond promoting GitLab. 

The long-term vision for meeting these goals is to launch a [branded editorial site](/handbook/marketing/growth-marketing/content/editorial-team/#tactics--launch-a-branded-editorial-site), to help establish GitLab as a trusted resource on software development, DevOps, collaboration, and remote work. 

In FY21-Q3 and -Q4 the areas of focus for the Editorial team are laying the foundations for launching the editorial site:

- Continue to monitor traffic to posts and document takeaways on performance to inform our planning of future posts
- Continue to document [best practices](/handbook/marketing/blog/#writing-blog-posts--best-practices) to enable contributors
- Improve SEO in new blog posts and selected past posts
- Experiment with curated content ("collections" of new and existing content on specific themes)
- Experiment with editorial-first content (i.e. a journalistic approach to stories and covering industry trends or news that is relevant to our audience, but may not place GitLab front and center)
- Collaborate with Brand and Digital on [blog redesign work](https://gitlab.com/groups/gitlab-com/-/epics/158)

### Content Marketing 

The content market team creates, writes, and publishes awareness and consideration level content for the website. The content focuses primarily on topics pages, web articles, eBooks, infographics, case studies, and white papers. The team works closely with various groups to strategize and develop content for all integrated marketing campaigns, including creating assets and providing awareness level insights for email marketing.

For Q3 and Q4 the content team will:

- Continue to create content for existing campaigns: VC&C, CI/CD, DevSecOps, GitOps.
- Continue cross-team collaboration for partner campaigns.
- Continue to produce case studies, web articles, and topics pages. 
- Work with content gap analysis to continue to create content accordingly.
- Create topics pages and web articles to coincide with product marketing use cases.

### [Digital Production](/handbook/marketing/growth-marketing/content/digital-production/) 

- Launch GitLab.tv with designated channels, inlcuding events, commuunity, product, and all-remote
- Launch a GitLab branded podcast
- Support AV-related needs and promos for corporate events
- Launch GitLab web series curating content from the Unfiltered channel 

### Website

Detail our plan & roadmap for content creation. Link to the CM handbook. Include plans to place/distribute content on the site. 

## Communication Overview 

### Issue trackers
 - [Global Content](https://gitlab.com/groups/gitlab-com/-/boards/1500703?&label_name[]=Global%20Content)
 - [Blog](https://gitlab.com/gitlab-com/www-gitlab-com/boards/804552?&label_name[]=blog%20post)
 - [Content by stage](https://gitlab.com/groups/gitlab-com/-/boards/1136104)

### Labels

We use the [mktg-status:: labels](/handbook/marketing/#boards-and-labels) to work status (triage, plan, WIP, design, review, scheduled).

### GitLab.com group labels

- `Corporate Marketing`: Department label. Add this label to all issues and epics.
- `Global Content`: General team label to track all issues related to the content team. Add this label to all issues and epics.
- `New Content Request`: Indicates a request for content creation, editing, or review.
- `Stage::Awareness`: Indicates content creation for the awareness stage of the buyer's journey.
- `Stage::Consideration`: Indicates content creation for the consideration stage of the buyer's journey.
- `Stage:: Purchase`: Indicates content creation for the purchase stage of the buyer's journey.
- `Stage:: Technical User`:
- `Editorial`: General label to track all issues related to the editorial team. This brings the issue in the Blog issue board for actioning.
- `Content Marketing`: Used by the Content Marketing team as a general label to track all issues related to content marketing. This brings the issue into the board for actioning.
- `Content Pillar`: Used by the Content Marketing Managers to indicate a content pillar epic. This label should **only be used on pillar epics.**
- `Gated Content`: Indicates content that requires MPM support. Use this label when creating a new epic for gated content (i.e. eBook, whitepaper, report).
- `Digital Production`: Used by the Content Marketing team as a general label to track all issues related to content marketing. This brings the issue into the board for actioning.
- `Blog UX`: Used to indicate a proposed change to the blog user experience.
- `Technical Post`: Indicates a blog post covering a technical engineering angle.

### www-gitlab-com project labels

- `Blog Post`: General label for blog posts. Add this label to all blog post issues and merge requests.
- `Blog Priority`: Indicates a blog post that is a high priority. These pitches and posts should be followed up on immediately.
- `Blog::Pitch`: Use this label when pitching a new blog post idea. All blog post issues must start here. Blog issues do not leave this stage until they have been assigned to a content team member.
- `Blog::Planning/In Progress`: Indicates blog posts that have been triaged to a content team member to work on.
- `Blog::Review`: Indicates blog posts that are ready for a content team review.
- `Blog::Scheduled`: Indicates blog posts that have been reviewed and are scheduled for publishing by the Managing Editor.
- `Blog::Waiting for author`: Indicates blog posts that have been reviewed by content team and are waiting for the author to address feedback or approve for scheduling.
- `CEO Interview`: Blog posts suggested by the CEO and need immediate action.
- `Error budget S1`: Indicates a blog post that has incurred 15 error budget points for providing than 2 working days' notice for a time-sensitive post.
- `Error budget S2`: Indicates a blog post that has incurred 10 error budget points for failure to submit complete, formatted MR a minimum of 2 working days ahead of publish date.
- `Error budget S3`: Indicates a blog post that has incurred 5 error budget points for submitting an MR without all required formatting, links, and images included.
- `Customer interview`: Use for blog posts that require or include a customer interview.
- `External outlet`: Use to indicate an article will be published outside of GitLab web domain.
- `Guest/Partner post`: Indicates a blog post that is being written and submitted from someone outside of GitLab.
- `Remote work post`: Indicates a blog post on the topic of remote work.
- `Sensitive`: Indicates a blog post with the potential to have wide-spread customer or community impact.
- `Unfiltered`: Indicates a blog post to be published on the GitLab unfiltered blog.

## Homepage Promotion Guidelines

The homepage hero is updated frequently to promote new content, events, and announcements. To use the homepage hero for promotion, follow the guidelines below. **Please submit your request as early as possible to get on the content calendar.**

1. [Open a homepage promotion request issue](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-website-homepage-promotion) **as early as possible.** If there is another promotion already scheduled, we may not be able to accommodate your request. 
1. Fill out the request completely. Provide as much information as possible for the copywriter. 
1. Suggested promotion period should not exceed two weeks. If you are promoting an event that is happening months into the future, we can promote the event more than once. Suggest a few 2-week periods for promotion. For example, if your event is happening in September and registration opens in June, you may want to promote your event for two weeks following initial promotion, and for two weeks leading up to the event. 

## Writing copy for the hompeage hero

1. The H1 should be 50 characters or less and include a "you" statement, speaking directly to the reader.
1. The H2 should be 65 characters or less and include a value statement (i.e. what the reader will gain). 
1. CTAs should be lead with an action verb and convey exactly what will be given in exchange for filling out the form.  

## Requesting content team support
*Need help finding relevant content to use in an email or to send to a customer?* Ask for help in the #content channel.*

### Requesting content and copy reviews

1. If the content you're looking for doesn't exist and it's a:
   1. Blog post: See the [blog handbook](/handbook/marketing/blog)
   1. Digital asset (eBook, infographic, report, etc.): open an issue in the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/) and label it `content marketing`
   1. Video: open an issue in the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/) and label it `video production`
1. If you need a copyedit, ping @erica. Please give at least 3 days' notice.

## Content production

The content team supports many cross-functional marketing initiatives. We aim for content production to be a month ahead of planned events and campaigns so that content is readily available for use. In accordance with our values of [iteration](/handbook/values/#iteration) and [transparency](/handbook/values/#transparency), we publish our proposed content plans at the beginning of each quarter. We don't hoard content for one big launch and instead plan sets of content to execute on each quarter and published each piece of content as it's completed.

Content production is determined and prioritized based on the following:

1. Corporate GTM themes
1. Integrated campaigns
1. Corporate marketing events
1. Newsworthiness
1. Brand awareness opportunity

We align our content production to pillars and topics to ensure we're creating a cohesive set of content for our audience to consume. Pillar content is multi-purpose and can be plugged into integrated campaigns, event campaigns, and sales plays.

### Content alignment

Content production is aligned to digital campaigns and product messaging.

**Definitions**

- Campaign/Value driver: A high-level GTM message that doesn't change often. Campaigns are tracked as `Parent Epics`.  
- Pillar: A story within a theme. Pillars are tracked as `Child Epics`.
- Set: A topical grouping of content to be executed within a specific timeframe. Sets are tracked as `Milestones`.
- Resource: An informative asset, such as an eBook or report, that is often gated.

## Content stage / buyer's journey definitions

### Awareness (Top)
Users and buyers realize that they have a problem or challenge which could be solved through some sort of outside software or service.  At this stage, they are trying to define the scope and the relative impact and size of the problem.  They probably do not yet have a solution identified, but are in the process of learning about potential solutions to their specific business problem.

In general, messaging in collateral and content should be focused on **educating them about the problems they are facing, the business impact of their problems, and the reality that others are successfully solving the same problem**.  This is an opportunity for them to learn that GitLab is an authority in addressing their domains.

### Consideration (Middle)
Users and buyers understand the problem they are trying to solve and the business impact/value of addressing the problem and are now actively seeking and evaluating potential remedies to their business issue.  Typically, they will be considering a range of approaches from better leveraging existing tools to investing in new technologies and vendors.  In this stage they are focused on identifying options that meet their specific requirements and needs.  While cost will be a consideration, they have not yet made a final decision about how to address their needs.  In this stage, they will be defining their budget and overall plans for implementing a solution.

In general, collateral and content designed to reach prospects in this stage of their journey should be focused on **positioning GitLab as a viable and compelling solution to their specific problem**.  

### Decision/Purchase (Bottom)
Users and buyers have concluded that they need to invest in solving a specific business problem and are now comparing and evaluating specific options.  In this stage, they are evaluating and comparing different options in order to identify the ideal solution for their specific situation.  They will consider Fit (technology, process, etc), implementation effort/cost, total cost of ownership and other factors to guide them in making a final selection.  

In general, collateral and content designed to reach prospects in this stage of their journey should be focused on key information that a buyer needs to **justify GitLab as their chosen solution**.

## Content library

Content is stored in PathFactory. To request access to PathFactory, submit an access request form.

The content library in PathFactory can be filtered by content type, funnel stage, and topic. Topics are listed and defined in the [marketing operations](/handbook/marketing/marketing-operations/pathfactory/).

Published content should be shared to the #content-updates channel and added to PathFactory with the appropriate tags.
