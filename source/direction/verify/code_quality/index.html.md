---
layout: markdown_page
title: "Category Direction - Code Quality"
description: "Automatically analyze your source code to surface issues and see if quality is improving or getting worse with the latest commit. Learn more!"
canonical_path: "/direction/verify/code_quality/"
---

- TOC
{:toc}

## Code Quality

Automatically analyze your static source code to surface issues and see if quality is improving or getting worse with the latest commit. Our vision for Code Quality is to provide actionable data across an organization to empower users to make quality visible with every commit and in every release.

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ACode%20Quality)
- [Overall Vision](/direction/ops/#verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

Interested in joining the conversation for this category? Please join us in the issues where we discuss this topic and can answer any questions you may have. Your contributions are more than welcome.

This page is maintained by the Product Manager for Testing, James Heimbuck ([E-mail](mailto:jheimbuck@gitlab.com))

## What's Next & Why

To move closer to our long term Vision of a Code Quality Dashboard we need to first ensure that users are only looking at Code Quality alerts that matter to them so they do not lose the important notices in a thousand point report. To enable that we will first allow users to customize their rules and [not show notices for rules marked ignore](https://gitlab.com/gitlab-org/gitlab/-/issues/221237).

## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is "Viable" (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

- [Don't show notices on rules marked ignore](https://gitlab.com/gitlab-org/gitlab/-/issues/221237)
- [Code quality notices on diffs/MRs](https://gitlab.com/gitlab-org/gitlab/issues/2526)
- [Prevent merge on code quality degredation](https://gitlab.com/gitlab-org/gitlab/-/issues/34982)

We may find in research that only some of these issues are needed to move the vision for this category forward. The work to move the vision is captured and being tracked in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/3686).

## Competitive Landscape

### Azure DevOps

Azure DevOps does not offer in-product quality testing in the same way we do with CodeClimate, but does have a number of easy to find and install plugins in their [marketplace](https://marketplace.visualstudio.com/search?term=code%20quality&target=AzureDevOps&category=All%20categories&sortBy=Relevance) that are both paid and free. Their [SonarQube plugin](https://marketplace.visualstudio.com/items?itemName=SonarSource.sonarqube) appears to be the most popular, though it seems to have some challenges with the rating.

In order to remain ahead of Azure DevOps, we should continue to push forward the feature capability of our own open-source integration with CodeClimate. Issues like [Code Quality report for default branch](https://gitlab.com/gitlab-org/gitlab-ee/issues/2766) moves both our vision forward as well as ensures we have a high quality integration in our product. To be successful here, though, we need to support formats Microsoft-stack developers use. The current Code Quality scanner has some limited scanning capaibility but the issue [Support C# code quality results direction](https://gitlab.com/gitlab-org/gitlab/issues/29218) extends this to be more in line with scanning provided for other languages. Because CodeClimate does not yet have deep .NET support, we may need to build something ourselves.

## Top Customer Success/Sales Issue(s)

An interesting suggestion from the CS team is to provide [Instance wide code statistics](https://gitlab.com/gitlab-org/gitlab/issues/8406), which is beyond just code quality but code quality could lead the way. We can see this being a view showing (for example) programming languages used, code quality statistics, and code coverage.

## Top Customer Issue(s)

The most popular feature issue is [Code quality report for default branch](https://gitlab.com/gitlab-org/gitlab/-/issues/2766) which will let developers get information about code quality issues in the default branch outside of a pipeline or MR context. 

Another top customer priority is to resolve the performance issues in parsing [large code quality reports](https://gitlab.com/gitlab-org/gitlab/-/issues/2737).

## Top Internal Customer Issue(s)

A top problem identified by our internal customers is that the code quality MR Widget and Report lack context about if code quality issues were introduced in the current Merge Reqeust. This makes it hard to incorporate that extra data into a Code Review. By resolving the issue [Show code quality notices on diffs/MRs](https://gitlab.com/gitlab-org/gitlab/issues/2526) we will help developers make better decisions about if a merge request should be merged, or if it needs further refinement.

## Top Vision Item(s)

Our vision for Code Quality is for it to become another rich signal of confidence for users of GitLab. This will be not just a signal of the quality of a change but one of many inputs like Code Coverage to be able to view a project at a high level and make decisions about what code needs attention, additional tests or refactoring, to bring it up to the quality requirements of the group. This long term vision is captured in the issues [Instance wide code statistics](https://gitlab.com/gitlab-org/gitlab/-/issues/8406) and [Code Quality Dashboard](https://gitlab.com/gitlab-org/gitlab/-/issues/207117).
