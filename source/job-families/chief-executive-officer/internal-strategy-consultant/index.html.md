---
layout: job_family_page
title: Internal Strategy Consultant
---

The Internal Strategy Consultant reports to the Chief of Staff and works to support the Chief of Staff and the CEO.
Internal consultants can quickly gather and analyze all data related to a problem and present next steps. 
They are the internal strategy consultants of GitLab who move from problem to problem in different functional areas.
This is a rotational role lasting 18 to 24 months. 

Please note this role is equivalent to the "Staff" level in terms of base salary and stock. 
The compensation calculator below intentionally only shows staff level compensation.

## Job Grade
  The Internal Strategy Consultant is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) role.

## Responsibilities
- Fill in knowledge gaps of the Chief of Staff
- Identify procedural gaps in existing workflows and work to resolve with optimization, automation, and data
- Support the creation of prepared materials (documents, decks) for the Chief of Staff and the CEO
- Translate practical needs into technical and/or business requirements
- Support special projects that are urgent and important
- Execute on projects and ongoing assignments for the Chief of Staff, as directed
- Demonstrate GitLab values in all work

## Requirements
- Detail-oriented forward thinker
- Excellent written, verbal, and technical communicator
- Experience making data-driven decisions
- Able to manage multiple tasks with competing timelines and deliverables
- Track record of leadership independent of role
- History of working cross-functionally to move projects forward
- Evidence of success in leading key business initiatives. Demonstrated ability to take a project from ideation through to implementation
- Experience at a strategy consulting firm is preferred
- Ideally, 2+ years experience in high growth startup environments
- Familiarity with the industry is a strong plus
- Ability to use GitLab

## Performance Indicators
- [Meetings shifted from CEO - hours spent in meetings decreases overtime by Chief of Staff stepping in, supported by Internal Consultants](/handbook/ceo/chief-of-staff-team/performance-indicators/#executive-time-for-the-ceo)
- [Throughput - Issues or merge requests closed as measurement of projects iterated on](/handbook/ceo/chief-of-staff-team/performance-indicators/#throughput-for-the-cost)

## Specializations
Internal consultants will almost always have specializations that support the gaps in knowledge of the Chief of Staff

### Data
- Experience stepping into a new data source and preparing new analyses
- A familiarity with proxy metrics where actual measurements aren’t available
- Ability to guide conversations related to strategic choices of performance indicators

### Operations
- Experience leading ongoing projects and initiatives
- Proven ability to manage cross-functional projects
- Track record of moving key initiatives from idea to delivery
- Experience in setting goals and driving toward quantifiable results

## Career Ladder
This is a cross-functional role, so team members will have exposure to broader parts of the organization. They may choose to join other teams. They may also be considered for the [Chief of Staff role](https://about.gitlab.com/job-families/chief-executive-officer/chief-of-staff/) as the Chief of Staff completes the rotation. 

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](https://about.gitlab.com/company/team/)

- Qualified candidates will be invited to schedule a 30 minute screening call with one of our global recruiters
- Next, candidates will be invited to schedule a 50 minute interview with a current Internal Strategy Consultant
- Then, candidates will be invited to schedule a 50 minute interview with the Chief of Staff and be asked to complete a homework assignment
- Next, candidates will be invited to schedule a 50 minute interview with a member of our Engineering team and be asked to complete a product assessment
- Then candidates will be invited to schedule a 50 minute interview with our CLO or another member of the executive team
- Finally, candidates will meet with our CEO who will conduct the final interview

As always, the interviews and screening call will be conducted via a video call. See more details about our hiring process on the [hiring handbook](https://about.gitlab.com/handbook/hiring/).
